import './styles/app.scss';

const $ = require('jquery');
global.$ = global.jQuery = $;

import '@popperjs/core';
require('bootstrap');

// start the Stimulus application
import './bootstrap';

console.debug('app.js loaded');

require('@fortawesome/fontawesome-free/js/all.js');