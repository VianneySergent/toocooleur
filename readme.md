# Too Cooleur

Too cooleur est un site présentant l'association de danse africaine "Too cooleur" de Chambéry.

## Environnement de développement

### Pré-requis

- PHP 7.4
- Composer
- Symfony CLI
- Docker
- Docker-compose
- nodejs et npm

Vous pouvez verifier les pré-requis (sauf Docker et Docker-compose) avec la commande suivante(de la CLI Symfony) :

```bash
symfony check:requirements
```

### Lancer l'environnement de développement

```bash
composer install
npm ou yarn install
npm ou yarn run build
docker-compose up-d
docker exec -it www_docker_symfony bash :
    -symfony serve -d
```

