<?php

namespace App\Repository;

use App\Entity\IsAdhesion;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method IsAdhesion|null find($id, $lockMode = null, $lockVersion = null)
 * @method IsAdhesion|null findOneBy(array $criteria, array $orderBy = null)
 * @method IsAdhesion[]    findAll()
 * @method IsAdhesion[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class IsAdhesionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, IsAdhesion::class);
    }

    // /**
    //  * @return IsAdhesion[] Returns an array of IsAdhesion objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('i.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?IsAdhesion
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
