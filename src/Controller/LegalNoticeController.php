<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class LegalNoticeController extends AbstractController
{
    /**
     * @Route("/mention-legale", name="legal-notice")
     */
    public function indexLegalNotice(): Response
    {
        return $this->render('legal_notice/legal-notice.html.twig');
    }
}
