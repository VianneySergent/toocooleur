<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class SiteMapController extends AbstractController
{
    /**
     * @Route("/sitemap.xml", name="sitemap", defaults={"_format"="xml"})
     */
    public function index(Request $request)
    {
        // on récupère le nom d'hote depuis l'URL
        $hostname = $request->getSchemeAndHttpHost();

        // On initialise un tableau pour lister les URLs
        $urls = []; // ou array()
        // on ajoutes les URLs "statiques"
        $urls[] = ['loc' => $this->generateUrl('Home')];
        $urls[] = ['loc' => $this->generateUrl('about')];
        $urls[] = ['loc' => $this->generateUrl('event')];
        $urls[] = ['loc' => $this->generateUrl('contact')];
        $urls[] = ['loc' => $this->generateUrl('gallery')];
        $urls[] = ['loc' => $this->generateUrl('login')];
        $urls[] = ['loc' => $this->generateUrl('register')];
        $urls[] = ['loc' => $this->generateUrl('legal-notice')];
        $urls[] = ['loc' => $this->generateUrl('team-benewell')];
        $urls[] = ['loc' => $this->generateUrl('team-office')];
        $urls[] = ['loc' => $this->generateUrl('team-artist')];
        $urls[] = ['loc' => $this->generateUrl('cultural_associative_partner')];
        $urls[] = ['loc' => $this->generateUrl('financial_partner')];
        $urls[] = ['loc' => $this->generateUrl('international_partner')];
        $urls[] = ['loc' => $this->generateUrl('patner_institution')];
         

        // fabriquer de la réponse
        $response = new Response(
            $this->renderView('sitemap/index.html.twig', [
                'urls' => $urls,
                'hostname' => $hostname
            ]),
            200
        );

        // ajout des entêtes HTTP
        $response->headers->set('Content-type', 'text/xml');

        // on envoie la réponse
        return $response;
    }
}