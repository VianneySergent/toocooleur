<?php

namespace App\Controller;

use App\Entity\CategorieArticle;
use App\Form\CategorieArticleType;
use App\Service\AlertServiceInterface;
use Symfony\Contracts\Cache\CacheInterface;
use Symfony\Component\HttpFoundation\Request;
use App\Repository\CategorieArticleRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/admin/categorie/article")
 */
class CategoryArticleController extends AbstractController
{
    /**
     * @Route("/", name="category_article_index", methods={"GET"})
     */
    public function indexCategoryArticle(
        CategorieArticleRepository $categorieArticleRepository): Response
        {
       

        return $this->render('administrator/category_article/category_article_index.html.twig', [            
            'categorie_articles' => $categorieArticleRepository->findAll()
        ]);
    }

    /**
     * @Route("/nouvel-catégorie-article", name="category_article_new", methods={"GET","POST"})
     */
    public function newCategoryArticle(Request $request,AlertServiceInterface $alertService): Response
    {
        $categorieArticle = new CategorieArticle();
        $form = $this->createForm(CategorieArticleType::class, $categorieArticle);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($categorieArticle);
            $entityManager->flush();
            $alertService->success('Vous avez ajouté une nouvelle catégorie Article');
            
            return $this->redirectToRoute('category_article_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('administrator/category_article/new_category_article.html.twig', [
            'categorie_article' => $categorieArticle,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="category_article_show", methods={"GET"})
     */
    public function showCategoryArticle(CategorieArticle $categorieArticle): Response
    {
        return $this->render('administrator/category_article/show_category_article.html.twig', [
            'categorie_article' => $categorieArticle,
        ]);
    }

    /**
     * @Route("/{id}/edition-catégorie-article", name="category_article_edit", methods={"GET","POST"})
     */
    public function editCategoryArticle(Request $request, CategorieArticle $categorieArticle): Response
    {
        $form = $this->createForm(CategorieArticleType::class, $categorieArticle);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('category_article_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('administrator/category_article/edit_category_article.html.twig', [
            'categorie_article' => $categorieArticle,
            'form' => $form
        ]);
    }

    /**
     * @Route("/{id}", name="category_article_delete", methods={"POST"})
     */
    public function deleteArticleCategory(Request $request, CategorieArticle $categorieArticle): Response
    {
        if ($this->isCsrfTokenValid('delete' . $categorieArticle->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($categorieArticle);
            $entityManager->flush();
        }

        return $this->redirectToRoute('category_article_home', [], Response::HTTP_SEE_OTHER);
    }
}