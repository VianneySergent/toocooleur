<?php

namespace App\Controller;

use App\Entity\Galerie;
use App\Repository\GalerieRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class CommonGalleryController extends AbstractController
{
    /**
     * @Route("/gallerie", name="gallery")
     */
    public function indexGallery(
        GalerieRepository $galerieRepository,
        PaginatorInterface $paginator,
        Request $request
    ): Response {
        $data = $galerieRepository->findAll();

        $galerie = $paginator->paginate(
            $data,
            $request->query->getInt('page', 1),
            8
        );

        return $this->render('gallery/gallery.html.twig', [
            'galeries' => $galerie,
        ]);
    }

    /**
     * @Route("gallery/{id}", name="gallery_show_user", methods={"GET"})
     */
    public function show(Galerie $galerie): Response
    {
        return $this->render('gallery/show_gallery.html.twig', [
            'galerie' => $galerie,
        ]);
    }
}