<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PartnerController extends AbstractController
{
    /**
     * @Route("/partenaire-associatif-culturel", name="cultural_associative_partner")
     */
    public function indexAssociativePartner(): Response
    {
        return $this->render('partner/cultural-associative-partner.html.twig');
    }
    /**
     * @Route("/partenaire-financier", name="financial_partner")
     */
    public function indexFinancialPartner(): Response
    {
        return $this->render('partner/financial-partner.html.twig');
    }
    /**
     * @Route("/partenaire-international", name="international_partner")
     */
    public function indexInternationalPartner(): Response
    {
        return $this->render('partner/international-partner.html.twig');
    }
    /**
     * @Route("/partenaire-institution", name="patner_institution")
     */
    public function indexInstitutionPartner(): Response
    {
        return $this->render('partner/institution-patner.html.twig');
    }
}
