<?php

namespace App\Controller;

use App\Entity\Pays;
use App\Form\PaysType;
use App\Repository\PaysRepository;
use App\Service\AlertServiceInterface;
use Symfony\Contracts\Cache\CacheInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("admin/pays")
 */
class CountryController extends AbstractController
{
    /**
     * @Route("/", name="country_index", methods={"GET"})
     */
    public function countryIndex(
        PaysRepository $paysRepository): Response {
      

        return $this->render('administrator/country/country_index.html.twig', [            
            'pays' =>$paysRepository->findAll()
        ]);
    }

    /**
     * @Route("/nouveau_pays", name="country_new", methods={"GET","POST"})
     */
    public function newCountry(Request $request,AlertServiceInterface $alertService): Response
    {
        $pay = new Pays();
        $form = $this->createForm(PaysType::class, $pay);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($pay);
            $entityManager->flush();
            $alertService->success('Vous avez ajouté un nouveau pays');
         

            return $this->redirectToRoute('country_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('administrator/country/new_country.html.twig', [
            'pay' => $pay,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="country_show", methods={"GET"})
     */
    public function showCountry(Pays $pay): Response
    {
        return $this->render('Administrator/country/show_country.html.twig', [
            'pay' => $pay,
        ]);
    }

    /**
     * @Route("/{id}/edition_pays", name="country_edit", methods={"GET","POST"})
     */
    public function editCountry(Request $request, Pays $pay): Response
    {
        $form = $this->createForm(PaysType::class, $pay);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('country_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('administrator/country/edit_country.html.twig', [
            'pay' => $pay,
            'form' => $form
        ]);
    }

    /**
     * @Route("/{id}", name="country_delete", methods={"POST"})
     */
    public function deleteCountry(Request $request, Pays $pay): Response
    {
        if ($this->isCsrfTokenValid('delete' . $pay->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($pay);
            $entityManager->flush();
        }

        return $this->redirectToRoute('country_index', [], Response::HTTP_SEE_OTHER);
    }
}