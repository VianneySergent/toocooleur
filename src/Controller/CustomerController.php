<?php

namespace App\Controller;

use App\Entity\Users;
use App\Form\PersoCustType;
use App\Repository\UsersRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\User\User;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
   * @Route("/users")
   */

class CustomerController extends AbstractController
{
 /*Index Customer*/

    /**
     * @Route("/", name="user-dashboard")
     */
    public function index_dashboard_customer(UsersRepository $userRepository): Response
    {
        if($this->getUser()) {
            $userid = $this->getUser()->getId();
        }

        return $this->render('users/user_dashboard.html.twig',[
            // 'users' => $userRepository->findAll(),
            'users' => $userRepository->findByUserId($userid)
        ]);
    }

     /**
     * @Route("/{user}/perso", name="user_perso", methods={"GET","POST"})
     */
    public function perso_user(UsersRepository $userRepository)
    {
        if($this->getUser())
        {
            $username = $this->getUser();
            $user = $userRepository->find($username);
        }

        return $this->redirectToRoute('user_edit_customer', array('user' => $user));
    }

    /**
     * @Route("/perso/index", name="perso_index", methods={"GET"})
     */
    public function index_perso(UsersRepository $userRepository): Response
    {

        if($this->getUser()) {
            $userid = $this->getUser()->getId();
        }

        return $this->render('users/perso/index.html.twig', ['users' => $userRepository->findByUserId($userid),
        ]);
    }

    /**
     * @Route("/perso/{id}/edit", name="perso_edit_user", methods={"GET","POST"})
     */
    public function perso_edit(Request $request,Users $user,UserPasswordEncoderInterface $passwordEncoder): Response
    {

        $form = $this->createForm(PersoCustType::class, $user);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {          
            
            $this->getDoctrine()->getManager()->flush();
            return $this->redirectToRoute('perso_index');
        }
        return $this->render('users/perso/edit.html.twig', ['user' => $user,'form' => $form->createView(),]);
    }
    
}