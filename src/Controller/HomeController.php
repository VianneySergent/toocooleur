<?php

namespace App\Controller;

use App\Entity\Article;
use App\Repository\ArticleRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Cache\CacheInterface;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="Home")
     */
    public function index(
        ArticleRepository $articleRepository,
        Request $request,
        PaginatorInterface $paginator        
    ) {      

        if ($this->getUser()) {
            $userrole = $this->getUser()->getRoles();
            $nbroles = count($userrole);
            if ($nbroles >= 1) {
                if ($userrole[0] == 'ROLE_ADMIN') {
                    $route = 'administrator-dashboard';
                }
            }
            if ($userrole[0] == 'ROLE_USER') {
                $route = 'user-dashboard';
            };
            return $this->redirectToRoute($route);
        }

        $donnees = $this->getDoctrine()->getRepository(Article::class)->findAll();

        $articles = $paginator->paginate(   
            $donnees,        
            $request->query->getInt('page', 1),
            3
        );     

       
        return $this->render('home/index.html.twig', [            
            'articles' => $articles,           
        ]);
    }

    /**
     * @Route("article/{id}", name="article_show_user", methods={"GET"})
     */
    public function showArticlePress(Article $article): Response
    {
        return $this->render('article/show_article.html.twig', [
            'article' => $article,
        ]);
    }
}