<?php

namespace App\Controller;

use App\Entity\Article;
use App\Repository\ArticleRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ArticleCommonController extends AbstractController
{
    // /**
    //  * @Route("/articles", name="article_common")
    //  */
    // public function index(): Response
    // {
    //     return $this->render('article_common/index.html.twig', [
    //         'controller_name' => 'ArticleCommonController',
    //     ]);
    // }

     /**
     * @Route("/articles", name="article", methods={"GET"})
     */
    public function indexArticle(ArticleRepository $articleRepository,PaginatorInterface $paginator,Request $request ): Response 
    {
      $donnees = $this->getDoctrine()->getRepository(Article::class)->findAll();

        $articles = $paginator->paginate(   
            $donnees,        
            $request->query->getInt('page', 1),
            3
        );     


        return $this->render('article/article_index.html.twig', [           
            // 'articles'=>$articleRepository->findAll(),
            'articles' => $articles
        ]);
    }
}