<?php

namespace App\Controller;

use App\Entity\Users;
use App\Form\ResetPassType;
use App\Repository\UsersRepository;
use App\Service\AlertServiceInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Csrf\TokenGenerator\TokenGeneratorInterface;

class SecurityController extends AbstractController
{
    /**
     * @Route("/connexion", name="login")
     */
    public function login(AuthenticationUtils $authenticationUtils,AlertServiceInterface $alertService)
    {         
        $error = $authenticationUtils->getLastAuthenticationError();
        $lastUsername = $authenticationUtils->getLastUsername();
        // $alertService->success('Votre inscription a été prise en compte.');
        // $alertService->success('Votre mot de passe ou votre email est incorrect.');       
        return $this->render('security/connexion.html.twig', [
            'last_username' => $lastUsername,
            'error' => $error,
        ]);
    }
    /**
     * @Route("/mot-de-passe-oublié", name="forgot_password")
     */
    public function forgotPass(
        Request $request,
        UsersRepository $usersRepo,
        MailerInterface $mailer,
        TokenGeneratorInterface $tokenGen,
        AlertServiceInterface $alertService
    ): Response {
        $form = $this->createForm(ResetPassType::class);
        // On traite le formulaire
        $form->handleRequest($request);
        // Si le formulaire est valide
        if ($form->isSubmitted() && $form->isValid()) {
            // On récupère les données
            $donnees = $form->getData();
            // On cherche si un utilisateur a cet email
            $user = $usersRepo->findOneByEmail($donnees['email']);
            // Si l'utilisateur n'existe pas
            if (!$user) {
                // On envoie un message flash
                $alertService->success('Cette adresse n\'existe pas');                 
                return $this->redirectToRoute('login');
            }
            // On génère un token
            $token = $tokenGen->generateToken();
            try {
                $user->setResetToken($token);
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($user);
                $entityManager->flush();
            } catch (\Exception $e) {
                $alertService->warning('Une erreur est survenue  :'. $e->getMessage());               
                return $this->redirectToRoute('login');
            }
            // On génère l'URL de réinitialisation de mot de passe
            // On envoie le message
            $message = (new TemplatedEmail())
                ->From('toocooleur@gmail.com')
                ->To($user->getEmail())
                ->subject('Mot de passe oublié')
                ->htmlTemplate('emails/forgot_password.html.twig')
                ->context([
                    'token' => $token,
                ]);
            // On envoie l'e-mail
            $mailer->send($message);
            // On crée le message flash
            $alertService->success('Un email de réinitialisation de mot de passe vous a été envoyé');             
            return $this->redirectToRoute('login');
        }
        return $this->render('security/forgot-password.html.twig', [
            'emailForm' => $form->createView(),
        ]);
    }
    /**
     * @Route("/deconnexion", name="logout")
     */
    public function logout()
    {
        throw new \Exception('Cette méthode peut être vide -
        elle sera interceptée par la clé de déconnexion de votre pare-feu.');
    }
    /**
     * @Route("/reinitialisation-mot-de-passe/{token}", name="reset_password")
     */
    public function resetPassword(
        $token,
        Request $request,
        UserPasswordEncoderInterface $passwordEncoder,
        AlertServiceInterface $alertService
    ) {
        // On cherche l'utilisateur avec le token fourni
        $user = $this->getDoctrine()->getRepository(Users::class)->findOneBy(['reset_token' => $token]);
        if (!$user) {
            $alertService->danger('tocken iconnu');
            return $this->redirectToRoute('login');
        }
        // Si le formulaire est envoyé en méthode POST
        if ($request->isMethod('POST')) {
            // On supprime le token
            $user->setResetToken(null);
            // On chiffre le mot de passe
            $user->setPassword($passwordEncoder->encodePassword($user, $request->request->get('password')));
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();
            $alertService->danger('Le mot de passe a été changé avec succès');            
            return $this->redirectToRoute('login');
        } else {
            return $this->render('security/reset_password.html.twig', ['token' => $token]);
        }
    }
}