<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TeamController extends AbstractController
{
    /**
     * @Route("/equipe-benevole", name="team-benewell")
     */
    public function benewellTeam(): Response
    {
        return $this->render('team/benewell.html.twig');
    }

     /**
     * @Route("/equipe-bureau", name="team-office")
     */
    public function associativeOffice(): Response
    {
        return $this->render('team/associative-office.html.twig');
    }

     /**
     * @Route("/equipe-artiste", name="team-artist")
     */
    public function artist(): Response
    {
        return $this->render('team/artist.html.twig');
    }
}
