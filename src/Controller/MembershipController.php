<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MembershipController extends AbstractController
{
    /**
     * @Route("/adhesion", name="membership")
     */
    public function index(): Response
    {
        return $this->render('encouragement/membership.html.twig', [
            'controller_name' => 'AdhesionController',
        ]);
    }
}