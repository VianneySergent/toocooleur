<?php

namespace App\Controller;

use App\Repository\ProduitsRepository;
use App\Service\CallApiService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Cache\CacheInterface;

class ShopController extends AbstractController
{
    /**
     * @Route("/boutique", name="shop")
     */
    public function indexhomeShop(
        ProduitsRepository $produitsRepository): Response {        
        
        return $this->render('shop/homeShop.html.twig', [           
            'produits'=> $produitsRepository->findAll()
        ]);
    }
}