<?php

namespace App\Controller;

use App\Entity\Galerie;
use App\Form\GalerieType;
use App\Repository\GalerieRepository;
use App\Service\AlertServiceInterface;
use Symfony\Contracts\Cache\CacheInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/admin/gallerie")
 */
class GalleryController extends AbstractController
{
    /**
     * @Route("/", name="gallery_index", methods={"GET"})
     */
    public function galleryIndex(
        GalerieRepository $galerieRepository ): Response {
      

        return $this->render('administrator/gallery/gallery_index.html.twig', [           
            'galeries' =>$galerieRepository->findAll()
        ]);
    }

    /**
     * @Route("/nouveau-media", name="gallery_new", methods={"GET","POST"})
     */
    public function newGallery(Request $request,AlertServiceInterface $alertService): Response
    {
        $galerie = new Galerie();
        $form = $this->createForm(GalerieType::class, $galerie);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($galerie);
            $entityManager->flush();
            $alertService->success('vous avez ajouté un nouveau média.');
            

            return $this->redirectToRoute('gallery_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('administrator/gallery/new_gallery.html.twig', [
            'galerie' => $galerie,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="gallery_show", methods={"GET"})
     */
    public function show(Galerie $galerie): Response
    {
        return $this->render('administrator/gallery/show_gallery.html.twig', [
            'galerie' => $galerie,
        ]);
    }

    /**
     * @Route("/{id}/edition-media", name="gallery_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Galerie $galerie): Response
    {
        $form = $this->createForm(GalerieType::class, $galerie);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('gallery_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('administrator/gallery/edit_gallery.html.twig', [
            'galerie' => $galerie,
            'form' => $form
        ]);
    }

    /**
     * @Route("/{id}", name="gallery_delete", methods={"POST"})
     */
    public function delete(Request $request, Galerie $galerie): Response
    {
        if ($this->isCsrfTokenValid('delete' . $galerie->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($galerie);
            $entityManager->flush();
        }

        return $this->redirectToRoute('gallery_index', [], Response::HTTP_SEE_OTHER);
    }
}