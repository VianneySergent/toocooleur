<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin")
 */
class AdministratorController extends AbstractController
{
    /**
     * @Route("/", name="administrator-dashboard")
     */
    public function indexDashboard(): Response
    {
        return $this->render('administrator/dashboard.html.twig');
    }
}
