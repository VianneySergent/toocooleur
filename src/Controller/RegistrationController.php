<?php

namespace App\Controller;

use App\Entity\Users;
use App\Form\RegistrationFormType;
use App\Repository\UsersRepository;
use App\Security\UsersAuthenticator;
use App\Service\AlertServiceInterface;
use App\Service\NotifyServiceInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Guard\GuardAuthenticatorHandler;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class RegistrationController extends AbstractController
{
    /**
     * @var NotifyServiceInterface
     */
    private $notifyService;

    /**
     * @param NotifyServiceInterface $notifyService
     */
    public function __construct(NotifyServiceInterface $notifyService)
    {
        $this->notifyService = $notifyService;
    }

    /**
     * @Route("/inscription", name="register")
     * @param Request $request
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param GuardAuthenticatorHandler $guardHandler
     * @param UsersAuthenticator $authenticator
     * @param MailerInterface $mailer
     * @return Response
     */
    public function register(
        Request $request,
        UserPasswordEncoderInterface $passwordEncoder,
        GuardAuthenticatorHandler $guardHandler,
        UsersAuthenticator $authenticator,
        MailerInterface $mailer
    ): Response
    {
        $user = new Users();

        $form = $this->createForm(RegistrationFormType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // encode the plain password
            $user->setPassword(
                $passwordEncoder->encodePassword(
                    $user,
                    $form->get('plainPassword')->getData()
                )
            );

            $user->setActivationToken(md5(uniqid('', true)));

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            $this->notifyService->notify(
                'toocooleur73@gmail.com',
                'toocooleur73@gmail.com',
                'Nouvelle inscription',
                'emails/add_account.html.twig'
            );

            // Envoie le mail d'activation
            $this->notifyService->notify(
                'toocooleur73@gmail.com',
                $user->getEmail(),
                'Activation de votre compte',
                'emails/activation.html.twig',
                [
                    'token' => $user->getActivationToken(),
                ]
            );

            return $guardHandler->authenticateUserAndHandleSuccess(
                $user,
                $request,
                $authenticator,
                'main' // firewall name in security.yaml
            );
        }

        return $this->render('registration/register.html.twig', [
            'registrationForm' => $form->createView()
        ]);
    }

    /**
     * @Route("/activation/{token}", name="activation")
     * @param $token
     * @param UsersRepository $usersRepo
     * @param AlertServiceInterface $alertService
     * @return Response
     */
    public function activation($token, UsersRepository $usersRepo, AlertServiceInterface $alertService): Response
    {
        // On vérifie si un utilisateur a ce token
        $user = $usersRepo->findOneBy(['activation_token' => $token]);

        // Si aucun utilisateur n'existe avec ce token
        if (!$user) {
            // Erreur 404
            throw $this->createNotFoundException('Cet utilisateur n\'existe pas');
        }

        // On supprime le token
        $user->setActivationToken(null);

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($user);
        $entityManager->flush();

        // On envoie un message flash
        $alertService->success('Vous avez activé votre compte avec succès');

        // On retoure à l'accueil
        return $this->redirectToRoute('Home');
    }
}