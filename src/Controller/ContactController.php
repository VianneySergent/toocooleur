<?php

namespace App\Controller;

use App\Form\ContactType;
use App\Service\AlertServiceInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ContactController extends AbstractController
{
    /**
     * @Route("/contact", name="contact")
     */
    public function index(Request $request, MailerInterface $mailer,AlertServiceInterface $alertService)
    {
        $form = $this->createForm(ContactType::class);
        $contact = $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $email = (new TemplatedEmail())
                ->from($contact->get('email')->getData())
                ->to('vianney.dev73@gmail.com')
                ->subject($contact->get('sujet')->getData())
                ->htmlTemplate('emails/contact.html.twig')
                ->context([
                    'mail' => $contact->get('email')->getData(),
                    'message' => $contact->get('message')->getData(),
                    'phone' => $contact->get('telephone')->getData()
                ]);
            $mailer->send($email);
            $alertService->success('Votre message a été envoyé, nous vous contacterons dès que possible.');         
            return $this->redirectToRoute('contact');
        }
        return $this->render('contact/contact_page.html.twig', [
            'ContactForm' => $form->createView()
        ]);
    }
}