<?php

namespace App\Controller;

use App\Entity\Evenement;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/")
 */
class ApiController extends AbstractController
{
   

    /**
     * @Route("/api/{id}/edit", name="api_event_edit", methods={"PUT"})
     */
    public function majEvent(?Evenement $Evenement, Request $request)
    {
        // On récupère les données
        $donnees = json_decode($request->getContent());

        if (
            isset($donnees->title) && !empty($donnees->title) &&
            isset($donnees->start) && !empty($donnees->start) &&
            isset($donnees->description) && !empty($donnees->description)          
        ) {
            // Les données sont complètes
            // On initialise un code
            $code = Response::HTTP_OK;

            // On vérifie si l'id existe
            if (!$Evenement) {
                // On instancie un rendez-vous
                $Evenement = new Evenement();

                // On change le code
                $code = Response::HTTP_CREATED;
            }

            // On hydrate l'objet avec les données
            $Evenement->setTitle($donnees->title);
            $Evenement->setDescription($donnees->description);
            $Evenement->setStart(new DateTime($donnees->start));
            
            if ($donnees->allDay) {
                $Evenement->setEnd(new DateTime($donnees->start));
            } else {
                $Evenement->setEnd(new DateTime($donnees->end));
            }
            
            $Evenement->setAllDay($donnees->allDay);

            $em = $this->getDoctrine()->getManager();
            $em->persist($Evenement);
            $em->flush();

            // On retourne le code
            return new Response('Ok', $code);
        } else {
            // Les données sont incomplètes
            return new Response('Donnée incomplète', 404);
        }
        
         return $this->render('api/index.html.twig', [
            'controller_name' => 'ApiController',
        ]);
       
    }
}