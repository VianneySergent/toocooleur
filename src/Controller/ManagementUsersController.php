<?php

namespace App\Controller;

use App\Entity\Users;
use App\Form\UsersType;
use App\Repository\UsersRepository;
use App\Service\AlertServiceInterface;
use Symfony\Contracts\Cache\CacheInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/admin/utilisateur")
 */
class ManagementUsersController extends AbstractController
{
    /**
     * @Route("/", name="Management_Users_index", methods={"GET"})
     */
    public function index(UsersRepository $usersRepository): Response
    {
        return $this->render('administrator/users_management/management_users_index.html.twig', [
            'users' => $usersRepository->findAll(),
        ]);
    }

    /**
     * @Route("/nouvel-utilisateur", name="user_management_new", methods={"GET","POST"})
     */
    public function new(Request $request,AlertServiceInterface $alertService): Response
    {
        $user = new Users();
        $form = $this->createForm(UsersType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();
            $alertService->success('Vous avez ajouté un nouvel utilisateur');

            return $this->redirectToRoute('Management_Users_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('administrator/users_management/new_management_users.html.twig', [
            'user' => $user,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/{id}", name="user_management_show", methods={"GET"})
     */
    public function show(Users $user): Response
    {
        return $this->render('administrator/users_management/show_management_users.html.twig', [
            'user' => $user,
        ]);
    }

    /**
     * @Route("/{id}/edition-utilisateur", name="user_management_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Users $user): Response
    {
        $form = $this->createForm(UsersType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('Management_Users_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('administrator/users_management/edit_management_users.html.twig', [
            'user' => $user,
            'form' => $form
        ]);
    }

    /**
     * @Route("/{id}", name="management_users_delete", methods={"POST"})
     */
    public function delete(Request $request, Users $user): Response
    {
        if ($this->isCsrfTokenValid('delete' . $user->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($user);
            $entityManager->flush();
        }

        return $this->redirectToRoute('Management_Users_index', [], Response::HTTP_SEE_OTHER);
    }
}