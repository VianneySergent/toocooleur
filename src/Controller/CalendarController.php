<?php

namespace App\Controller;

use App\Entity\Evenement;
use App\Form\EvenementsType;
use App\Service\AlertServiceInterface;
use App\Repository\EvenementsRepository;
use Symfony\Contracts\Cache\CacheInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/admin/calendar")
 */
class CalendarController extends AbstractController
{
    /**
     * @Route("/", name="calendar_index", methods={"GET"})
     */
    public function index(
        EvenementsRepository $EvenementsRepository): Response {
       

        return $this->render('administrator/calendar/calendar_index.html.twig', [            
            'evenements' => $EvenementsRepository->findAll()
        ]);
    }

    /**
     * @Route("/nouvel-evenement", name="calendar_new", methods={"GET","POST"})
     */
    public function new(Request $request,AlertServiceInterface $alertService): Response
    {
        $evenement = new Evenement();
        $form = $this->createForm(EvenementsType::class, $evenement);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($evenement);
            $entityManager->flush();
            // $alertService->success('Vous avez ajouté un nouvel évènement');
           

            return $this->redirectToRoute('calendar_index');
        }

        return $this->render('administrator/calendar/new_calendar.html.twig', [
            'evenement' => $evenement,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="calendar_show", methods={"GET"})
     */
    public function show(Evenement $evenement): Response
    {
        return $this->render('administrator/calendar/show_calendar.html.twig', [
            'evenement' => $evenement,
        ]);
    }

    /**
     * @Route("/{id}/edition", name="calendar_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Evenement $evenement): Response
    {
        $form = $this->createForm(EvenementsType::class, $evenement);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('calendar_index');
        }

        return $this->render('administrator/calendar/edit_calendar.html.twig', [
            'evenement' => $evenement,
            'form' => $form
        ]);
    }

    /**
     * @Route("/{id}", name="calendar_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Evenement $evenement): Response
    {
        if ($this->isCsrfTokenValid('delete' . $evenement->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($evenement);
            $entityManager->flush();
        }

        return $this->redirectToRoute('calendar_index');
    }
}