<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Gregwar\CaptchaBundle\Type\CaptchaType;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\File;
use Beelab\Recaptcha2Bundle\Form\Type\RecaptchaType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Beelab\Recaptcha2Bundle\Validator\Constraints\Recaptcha2;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType as TypeTextType;

class ContactType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            // ->add('nom')           
            ->add('nom', TypeTextType::class)
            ->add('prenom',TypeTextType::class,array(
                'label'=> 'Prénom'
            ))
            ->add('telephone', NumberType::class, array(
                'label'=> 'Téléphone'
            ))
             ->add(
                'email',
                EmailType::class,
                array('required' => true)
            )
            ->add('sujet', ChoiceType::class, [
                'choices' => [                  
                    'Suppression Compte' => 'Suppression Compte',
                    'Problème Technique' =>  'Problème Technique'
                ],
                'required' => false,
            ])
            // ->add('piecejointe', FileType::class, [
            //     'label' => 'Envoie de pièce jointe (PDF file, image)',
            //     // unmapped means that this field is not associated to any entity property
            //     'mapped' => false,
            //     // make it optional so you don't have to re-upload the PDF file
            //     // every time you edit the Product details
            //     // unmapped fields can't define their validation using annotations
            //     // in the associated entity, so you can use the PHP constraint classes
            //     'constraints' => [
            //         new File([
            //             'maxSize' => '1024k',
            //             'mimeTypes' => [
            //                 'application/pdf',
            //                 'application/x-pdf',
            //                 'image/jpeg',
            //                 'image/png'
            //             ],
            //             'mimeTypesMessage' => 'Please upload a valid PDF document',
            //         ])
            //     ],
            // ])
            ->add('message', CKEditorType::class)
            // ->add('captcha', CaptchaType::class, array(
            //     'width' => 200,
            //     'height' => 50,
            //     'length' => 6,
            //     'required' => true
            // ))
            // ->add('captcha', RecaptchaType::class, [
            //     // You can use RecaptchaSubmitType
            //     // "groups" option is not mandatory
            //     'constraints' => new Recaptcha2(['groups' => ['create']]),
            // ])
            ->add('Envoyer', SubmitType::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}