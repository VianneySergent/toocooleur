<?php

namespace App\Form;

use App\Entity\Users;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class UsersType extends AbstractType
{
    public const CHOICE_YES_OR_NO = [
        'Oui' => true,
        'Non' => false,
    ];

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     * @return void
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('nom')
            ->add('prenom')
            ->add('username')
            ->add('email')
            ->add('adresse')
            ->add('ville')
            ->add('code_postal')
            ->add('complement_adresse')
            ->add('telephone')
            ->add('isAdhesion', ChoiceType::class, [
                'choices' => self::CHOICE_YES_OR_NO,
                'placeholder' => 'Choisir une option ...',
            ])
        //    ->add('pays') 
        ;
    }

    /**
     * @param OptionsResolver $resolver
     * @return void
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Users::class,
        ]);
    }
}