<?php

namespace App\Form;


use App\Entity\Users;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class PersoCustType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        //     ->add('genres',ChoiceType::class,array(
        //     'choices'  => array(
        //         'Monsieur' => 'Monsieur',
        //         'Madame' => 'Madame',                    
        //     ),
        //     'expanded' => true,
        //     'multiple' => false
        // ))             
            ->add('email') 
            ->add('complement_adresse')  
            ->add('adresse')
            ->add('telephone')
            ->add('ville')
            ->add('code_postal')
            ->add('prenom')
            ->add('nom') 
            ->add('username')           
            ->add('valider', SubmitType::class)                   
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Users::class,
        ]);
    }
}