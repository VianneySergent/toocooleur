<?php

namespace App\Form;

use App\Entity\Users;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\IsTrue;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class RegistrationFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', EmailType::class, array(
                'label'=> false,
                'attr' =>array(
                    'placeholder'=> 'Email *'
                )                 
            ))
            ->add('nom', TextType::class, array(
                'label'=> false,
                'attr'=>array(
                    'placeholder' =>'Nom *'
                )
            ))
            ->add('prenom', TextType::class, array(
                'label'=>false,
                'attr'=>array(
                    'placeholder'=>'Prénom *'
                )
            ))
            ->add('validation_inscription', CheckboxType::class, [
                'mapped' => false,
                'constraints' => [
                    new IsTrue([
                        'message' => 'Vous devez accepter nos conditions.',
                    ]),
                ],
            ])
            ->add('plainPassword', PasswordType::class, [
                // instead of being set onto the object directly,
                // this is read and encoded in the controller
                'mapped' => false,
                'constraints' => [
                    new NotBlank([
                        'message' => 'Veuillez entrer un mot de passe',
                    ]),
                    new Length([
                        'min' => 6,
                        'minMessage' => 'Your password should be at least {{ limit }} characters',
                        // max length allowed by Symfony for security reasons
                        'max' => 4096,
                    ]),                   
                ],
                'label'=>false,
                'attr'=> array(
                    'placeholder'=>'Mot de passe *'
                )                
            ])
            ->add('username', TextType::class,array(
                'label'=>false,
                'attr'=>array(
                    'placeholder'=>'Nom utilisateur *'
                )
            ))
            ->add('adresse', TextType::class, array(
                'label'=>false,
                'attr'=> array(
                    'placeholder'=> 'Adresse *'
                )
            ))
            ->add('telephone', TextType::class, array(
                'label'=>false,
                'attr'=> array(
                    'placeholder'=> 'Téléphone *'
                )
            ))
            ->add('code_postal', TextType::class, array(
                'label'=>false,
                'attr'=> array(
                    'placeholder'=> 'Code postal'
                )
            ))
            ->add('ville', TextType::class, array(
                'label'=>false,
                'attr'=> array(
                    'placeholder'=> 'Ville *'
                )
            ))
            ->add('complement_adresse', TextType::class, array(
                'required'   => false,
                'label'=> false,
                'attr'=>array(
                    'placeholder'=> 'Complément adresse'
                )
            ))
            // ->add('pays', TextType::class, array(
            //     'required'   => false
            // ))
            // ->add('valider', SubmitType::class)
          
        ;
    }
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Users::class,
        ]);
    }
}