<?php

namespace App\Service;

interface NotifyServiceInterface
{
    /**
     * @param string $from
     * @param string $to
     * @param string $subject
     * @param string $htmlTemplate
     * @param array $context
     * @return void
     */
    public function notify(string $from, string $to, string $subject, string $htmlTemplate, array $context = []): void;
}
