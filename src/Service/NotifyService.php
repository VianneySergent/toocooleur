<?php

namespace App\Service;

use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;

class NotifyService implements NotifyServiceInterface
{
    /**
     * @var MailerInterface
     */
    private $mailer;

    public function __construct(MailerInterface $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * @inheritDoc
     * @throws TransportExceptionInterface
     */
    public function notify(string $from, string $to, string $subject, string $htmlTemplate, array $context = []): void
    {
        $message = (new TemplatedEmail())
            ->From($from)
            ->To($to)
            ->subject($subject)
            ->htmlTemplate($htmlTemplate)
            ->context($context)
        ;

        $this->mailer->send($message);
    }
}
