<?php

namespace App\Entity;

use App\Repository\ArticleRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\HttpFoundation\File\File;

/**
 * @ORM\Entity(repositoryClass=ArticleRepository::class)
 * @Vich\Uploadable
 */
class Article
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Titre;

    /**
     * @ORM\Column(type="string", length=10000)
     */
    private $description;

    /**
     * @ORM\Column(type="date")
     */
    private $Date_de_publication;

    /**
     * @ORM\ManyToMany(targetEntity=CategorieArticle::class, inversedBy="articles")
     */
    private $Categories;


    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $imagesArticle;

     /**
     * @Vich\UploadableField(mapping="imagesArticle", fileNameProperty="imagesArticle")
     * @var File
     */
    private $imageFile;

    public function __construct()
    {
        $this->Categories = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitre(): ?string
    {
        return $this->Titre;
    }

    public function setTitre(string $Titre): self
    {
        $this->Titre = $Titre;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getDateDePublication(): ?\DateTimeInterface
    {
        return $this->Date_de_publication;
    }

    public function setDateDePublication(\DateTimeInterface $Date_de_publication): self
    {
        $this->Date_de_publication = $Date_de_publication;

        return $this;
    }

    /**
     * @return Collection|CategorieArticle[]
     */
    public function getCategories(): Collection
    {
        return $this->Categories;
    }

    public function addCategory(CategorieArticle $category): self
    {
        if (!$this->Categories->contains($category)) {
            $this->Categories[] = $category;
        }

        return $this;
    }

    public function removeCategory(CategorieArticle $category): self
    {
        $this->Categories->removeElement($category);

        return $this;
    }

    public function getImagesArticle(): ?string
    {
        return $this->imagesArticle;
    }

    public function setImagesArticle(?string $imagesArticle): self
    {
        $this->imagesArticle = $imagesArticle;

        return $this;
    }

    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;

        if ($image) {
            $this->updated_at = new \DateTime('now');
        }
    }
    public function getImageFile()
    {
        return $this->imageFile;
    }
}