<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\GalerieRepository;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\HttpFoundation\File\File;

/**
 * @ORM\Entity(repositoryClass=GalerieRepository::class)
 * @Vich\Uploadable
 */
class Galerie
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $Date_Publication;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $youtubeUrl;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $imagesGalerie;

     /**
     * @Vich\UploadableField(mapping="imagesGalerie", fileNameProperty="imagesGalerie")
     * @var File
     */
    private $imageFile;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDatePublication(): ?\DateTimeInterface
    {
        return $this->Date_Publication;
    }

    public function setDatePublication(?\DateTimeInterface $Date_Publication): self
    {
        $this->Date_Publication = $Date_Publication;

        return $this;
    }

    public function getYoutubeUrl(): ?string
    {
        return $this->youtubeUrl;
    }

    public function setYoutubeUrl(?string $youtubeUrl): self
    {
        $this->youtubeUrl = $youtubeUrl;

        return $this;
    }

    public function getImagesGalerie(): ?string
    {
        return $this->imagesGalerie;
    }

    public function setImagesGalerie(?string $imagesGalerie): self
    {
        $this->imagesGalerie = $imagesGalerie;

        return $this;
    }

    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;

        if ($image) {
            $this->updated_at = new \DateTime('now');
        }
    }
    public function getImageFile()
    {
        return $this->imageFile;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }
}
