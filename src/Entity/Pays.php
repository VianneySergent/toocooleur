<?php

namespace App\Entity;

use App\Repository\PaysRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PaysRepository::class)
 */
class Pays
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * @ORM\OneToMany(targetEntity=Users::class, mappedBy="pays")
     */
    private $pays;

    public function __construct()
    {
        $this->pays = new ArrayCollection();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getNom(): ?string
    {
        return $this->nom;
    }

    /**
     * @param string $nom
     * @return $this
     */
    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * @return Collection|Users[]
     */
    public function getPays(): Collection
    {
        return $this->pays;
    }

    /**
     * @param Users $pays
     * @return $this
     */
    public function addPays(Users $pays): self
    {
        if (!$this->pays->contains($pays)) {
            $this->pays[] = $pays;
            $pays->setPays($this);
        }

        return $this;
    }

    /**
     * @param Users $pays
     * @return $this
     */
    public function removePays(Users $pays): self
    {
        if ($this->pays->removeElement($pays) && $pays->getPays() === $this) {
            $pays->setPays(null);
        }

        return $this;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->nom;
    }
}
