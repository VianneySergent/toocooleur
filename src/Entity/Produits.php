<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\ProduitsRepository;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\HttpFoundation\File\File;
use Doctrine\Common\Collections\ArrayCollection;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass=ProduitsRepository::class)
 *  @Vich\Uploadable
 */
class Produits
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="datetime")
     */
    private $datePublication;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dateMiseAJour;

    /**
     * @ORM\Column(type="string", length=255)
     * @var string
     */
    private $imagesProduits;

    /**
     * @Vich\UploadableField(mapping="imagesProduits", fileNameProperty="imagesProduits")
     * @var File
     */
    private $imageFile;

    /**
     * @ORM\Column(type="float")
     */
    private $Prix;

    /**
     * @ORM\ManyToMany(targetEntity=ProduitCategories::class, inversedBy="produits")
     */
    private $Categories;

    public function __construct()
    {
        $this->Categories = new ArrayCollection();
    }
    public function getId(): ?int
    {
        return $this->id;
    }
    public function getNom(): ?string
    {
        return $this->nom;
    }
    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }
    public function getDescription(): ?string
    {
        return $this->description;
    }
    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }
    public function getDatePublication(): ?\DateTimeInterface
    {
        return $this->datePublication;
    }
    public function setDatePublication(\DateTimeInterface $datePublication): self
    {
        $this->datePublication = $datePublication;

        return $this;
    }
    public function getDateMiseAJour(): ?\DateTimeInterface
    {
        return $this->dateMiseAJour;
    }
    public function setDateMiseAJour(\DateTimeInterface $dateMiseAJour): self
    {
        $this->dateMiseAJour = $dateMiseAJour;

        return $this;
    }
    public function getimagesProduits()
    {
        return $this->imagesProduits;
    }
    public function setimagesProduits($imagesProduits)
    {
        $this->imagesProduits = $imagesProduits;

        return $this;
    }
    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;

        if ($image) {
            $this->updated_at = new \DateTime('now');
        }
    }
    public function getImageFile()
    {
        return $this->imageFile;
    }
    public function getPrix(): ?float
    {
        return $this->Prix;
    }
    public function setPrix(float $Prix): self
    {
        $this->Prix = $Prix;

        return $this;
    }
    /**
     * @return Collection|ProduitCategories[]
     */
    public function getProduitCategories(): Collection
    {
        return $this->produitCategories;
    }
    public function addProduitCategory(ProduitCategories $produitCategory): self
    {
        if (!$this->produitCategories->contains($produitCategory)) {
            $this->produitCategories[] = $produitCategory;
            $produitCategory->addProduit($this);
        }

        return $this;
    }
    public function removeProduitCategory(ProduitCategories $produitCategory): self
    {
        if ($this->produitCategories->removeElement($produitCategory)) {
            $produitCategory->removeProduit($this);
        }

        return $this;
    }
    /**
     * @return Collection|ProduitCategories[]
     */
    public function getCategories(): Collection
    {
        return $this->Categories;
    }
    public function addCategory(ProduitCategories $category): self
    {
        if (!$this->Categories->contains($category)) {
            $this->Categories[] = $category;
        }

        return $this;
    }
    public function removeCategory(ProduitCategories $category): self
    {
        $this->Categories->removeElement($category);

        return $this;
    }
}
